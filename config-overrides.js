import {alias} from 'react-app-rewire-alias';

// eslint-disable-next-line no-unused-vars
export default function override(config, env) {
    alias({
        '@components': './src/components',
        '@containers': './src/containers',
        '@constants': './src/constants',
        '@UI': './src/components/UI'
    })(config);

    return config;
}
